import React from 'react';
import {BrowserRouter, Switch} from 'react-router-dom';

import SideMenu from '../components/SideMenu';
import HomePage from '../pages/HomePage';
import {PrivateRoute} from './PrivateRoute';
import ProtectedPage from '../pages/ProtectedPage';
import {useKeycloak} from "@react-keycloak/web";
import Header from "../components/Header";
import DevisTable from "../components/DevisTableComponent";
import DevisComponent from "../components/DevisComponent";
import DevisConsultComponent from "../components/DevisConsultComponent";
import GammeTableComponent from "../components/GammeTableComponent";
import ModuleTableComponent from "../components/ModuleTableComponent";
import ComposantTableComponent from "../components/ComposantTableComponent";
import ClientTableComponent from "../components/ClientTableComponent";
import GammeConsultComponent from "../components/GammeConsultComponent";
import ModuleConsultComponent from "../components/ModuleConsultComponent";
import ComposantConsultComponent from "../components/ComposantConsultComponent";
import ClientConsultComponent from "../components/ClientConsultComponent";
import GammeEditComponent from "../components/GammeEditComponent";
import ClientEditComponent from "../components/ClientEditComponent";


export const AppRouter = () => {
    const {initialized} = useKeycloak();

    if (!initialized) {
        return <h3>Loading...</h3>;
    }
    return (
        <BrowserRouter>
            <Header/>
            <div className="p-d-flex">
                <div className="p-col-2">
                    <SideMenu/>
                </div>
                <div className="p-col">
                    <Switch>
                        <PrivateRoute exact path="/" component={HomePage}/>
                        <PrivateRoute roles={['commercial', 'client']} exact path="/devis" component={DevisTable}/>
                        <PrivateRoute roles={['commercial', 'client']} exact path="/devis/consult" component={DevisConsultComponent}/>
                        <PrivateRoute roles={['commercial']} path="/devis/nouveau" component={DevisComponent}/>
                        <PrivateRoute roles={['bureau_etude']} exact path="/gammes" component={GammeTableComponent}/>
                        <PrivateRoute roles={['bureau_etude']} exact path="/gamme/consult" component={GammeConsultComponent}/>
                        <PrivateRoute roles={['bureau_etude']} exact path="/gamme/nouveau" component={GammeEditComponent}/>
                        <PrivateRoute roles={['bureau_etude']} exact path="/modules" component={ModuleTableComponent}/>
                        <PrivateRoute roles={['bureau_etude']} exact path="/module/consult" component={ModuleConsultComponent}/>
                        <PrivateRoute roles={['bureau_etude']} exact path="/composants" component={ComposantTableComponent}/>
                        <PrivateRoute roles={['bureau_etude']} exact path="/composant/consult" component={ComposantConsultComponent}/>
                        <PrivateRoute roles={['commercial']} exact path="/clients" component={ClientTableComponent}/>
                        <PrivateRoute roles={['commercial']} exact path="/client/consult" component={ClientConsultComponent}/>
                        <PrivateRoute roles={['commercial']} exact path="/client/nouveau" component={ClientEditComponent}/>
                    </Switch>
                </div>
            </div>
        </BrowserRouter>
    );
};
