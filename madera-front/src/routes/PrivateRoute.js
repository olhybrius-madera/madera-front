import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import useAuthorization from "../hooks/useAuthorization";
import {useKeycloak} from "@react-keycloak/web";


export function PrivateRoute({component: Component, roles, ...rest}) {
    const {keycloak} = useKeycloak();
    const hasAccess = useAuthorization(roles);

    return (
        <Route
            {...rest}
            render={props => {
                if (!roles) {
                    return keycloak.authenticated ? <Component {...props} /> : keycloak.login()
                }
                return hasAccess ? <Component {...props} /> : <Redirect to={{pathname: '/',}}/>
            }}
        />
    )
}
