import {Panel} from "primereact/panel";
import {Button} from "primereact/button";

const ClientConsultComponent = (props) => {
    const {client} = props.location.state;


    return (
        <div>
            <Panel header="Informations" className="p-mb-2" toggleable>
                <p> Nom: {client.nom} </p>
                <p> Prénom : {client.prenom} </p>
                <p> Adresse : {client.adresse}</p>
                <p> Ville
                    : {`${client.code_postal ?? ''} ${client.ville ?? ''}`}</p>
                <p> Téléphone : {client.telephone}</p>
                <p> Mail : {client.mail}</p>
            </Panel>
            <div className="p-d-flex p-jc-end">
                <Button label="Retour" onClick={() => props.history.goBack()}/>
            </div>
        </div>);
}
export default ClientConsultComponent;
