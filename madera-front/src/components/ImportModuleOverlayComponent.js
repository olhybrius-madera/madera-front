import {OverlayPanel} from 'primereact/overlaypanel';
import {FileUpload} from 'primereact/fileupload';
import {Toast} from "primereact/toast";
import React, {useRef} from "react";
import {useKeycloak} from "@react-keycloak/web";
import {importerModule} from "../services/ConfigurationService";

const ImportModuleOverlayComponent = React.forwardRef((props, ref) => {
    const toast = useRef(null);
    const {keycloak} = useKeycloak();

    const showToast = (severityValue, summaryValue, detailValue) => {
        toast.current.show({
            severity: severityValue, summary: summaryValue, detail: detailValue
        });
    }

    const moduleUploadHandler = async (event) => {
        const {files} = event;
        try {
            await importerModule(keycloak.token, files[0]);
            ref.current.hide()
        } catch (error) {
            showToast('error', 'Une erreur est survenue', error.response.data.detail);
        }
    }

    return (
        <OverlayPanel ref={ref} onHide={props.onHide}>
            <Toast ref={toast}/>
            <FileUpload chooseLabel="Choisir"
                        uploadLabel="Téléverser"
                        cancelLabel="Annuler"
                        customUpload
                        uploadHandler={moduleUploadHandler}
                        emptyTemplate={<p className="p-m-0">Glissez-déposez vos fichiers ici.</p>}/>
        </OverlayPanel>
    )
});
export default ImportModuleOverlayComponent;
