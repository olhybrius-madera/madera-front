import {useKeycloak} from "@react-keycloak/web";
import {useHistory} from "react-router-dom";
import React, {useEffect, useRef, useState} from "react";
import {getAllGammes} from "../services/DevisService";
import {Button} from "primereact/button";
import {DataTable} from "primereact/datatable";
import {Column} from "primereact/column";
import {getAllModules} from "../services/ConfigurationService";
import ImportModuleOverlayComponent from "./ImportModuleOverlayComponent";
import {Toast} from "primereact/toast";

const ModuleTableComponent = () => {
    const {keycloak} = useKeycloak();
    const history = useHistory();
    const [gammes, setGammes] = useState([]);
    const [modules, setModules] = useState([]);
    const ref = React.createRef();
    const toast = useRef(null);

    useEffect(() => {
            (async () => {
                const data = await getAllGammes(keycloak.token);
                setGammes(data);
            })();
        }, []
    )

    useEffect(() => {
            (async () => {
               await loadModules();
            })();
        }, []
    )

    const loadModules = async () => {
        const data = await getAllModules(keycloak.token);
        setModules(data);
    }

    const formatMontant = (value) => {
        return `${String(value).replace('.', ',')} €`;
    };

    const montantBodyTemplate = (rowData) => {
        const {prix} = rowData;
        return formatMontant(prix);
    };

    const gammeBodyTemplate = (rowData) => {
        const {gamme} = rowData;
        const gammeInfos = gammes.find(g => g.uuid === gamme);
        return `${gammeInfos?.libelle ?? ''}`
    }

    const actionBodyTemplate = (rowData) => {
        return (
            <Button type="button" icon="pi pi-search" className="p-button-rounded"
                    onClick={() => history.push('/module/consult', {module: rowData})}/>
        );
    }

    const showToast = (severityValue, summaryValue, detailValue) => {
        toast.current.show({
            severity: severityValue, summary: summaryValue, detail: detailValue
        });
    }

    const onOverlayHide = async () => {
        await loadModules();
        showToast('success', `Module importé avec succès !`, '')
    }

    return (
        <div>
            <Toast ref={toast}/>
            <ImportModuleOverlayComponent ref={ref} onHide={onOverlayHide}/>
            <div className="p-d-flex">
                <Button className="p-mb-2 p-ml-auto" label="Importer un module" onClick={(e) => ref.current.toggle(e)}/>
            </div>
            <div className="p-grid p-justify-around">
                <div className="p-col">
                    <DataTable value={modules} className="p-datatable-striped" autoLayout={true} removableSort>
                        <Column field="libelle" header="Libellé" sortable/>
                        <Column field="unite" header="Unité" sortable/>
                        <Column field="prix" header="Prix" sortable body={montantBodyTemplate}/>
                        <Column field="gamme" header="Gamme" body={gammeBodyTemplate} sortable/>
                        <Column body={actionBodyTemplate}/>
                    </DataTable>
                </div>
            </div>
        </div>
    );
}
export default ModuleTableComponent;
