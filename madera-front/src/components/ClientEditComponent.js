import {Panel} from "primereact/panel";
import {InputText} from "primereact/inputtext";
import {Button} from "primereact/button";
import {useHistory} from "react-router-dom";
import {useKeycloak} from "@react-keycloak/web";
import React, {useRef, useState} from "react";
import {soumettreFicheClient} from "../services/ActeursService";
import {Toast} from "primereact/toast";
import {validerCodePostal, validerEmail} from "../utils/validators";

const ClientEditComponent = () => {
    const history = useHistory();
    const {keycloak} = useKeycloak();
    const [nom, setNom] = useState('');
    const [prenom, setPrenom] = useState('');
    const [adresse, setAdresse] = useState('');
    const [complementAdresse, setComplementAdresse] = useState('');
    const [codePostal, setCodePostal] = useState('');
    const [ville, setVille] = useState('');
    const [telephone, setTelephone] = useState('');
    const [mail, setMail] = useState('');
    const toast = useRef(null);
    const codePostalValide = codePostal && validerCodePostal(codePostal)
    const adresseMailValide = mail && validerEmail(mail)
    const formulaireValide = nom && prenom && adresse && codePostalValide && ville && telephone && adresseMailValide;

    const onNomChange = (e) => {
        setNom(e.target.value);
    }

    const onPrenomChange = (e) => {
        setPrenom(e.target.value);
    }

    const onAdresseChange = (e) => {
        setAdresse(e.target.value);
    }

    const onComplementAdresseChange = (e) => {
        setComplementAdresse(e.target.value);
    }

    const onCodePostalChange = (e) => {
        setCodePostal(e.target.value);
    }

    const onVilleChange = (e) => {
        setVille(e.target.value);
    }

    const onTelephoneChange = (e) => {
        setTelephone(e.target.value);
    }

    const onMailChange = (e) => {
        setMail(e.target.value);
    }

    const showToast = (severityValue, summaryValue, detailValue) => {
        toast.current.show({
            severity: severityValue, summary: summaryValue, detail: detailValue
        });
    }

    const enregistrerClient = async () => {
        const payload = {
            nom,
            prenom,
            adresse,
            complement_adresse: complementAdresse,
            code_postal: codePostal,
            ville,
            telephone,
            mail
        };
        try {
            await soumettreFicheClient(keycloak.token, payload);
            showToast('success', `Fiche client enregistrée avec succès !`, '');
        } catch (error) {
            showToast('error', 'Une erreur est survenue', error.response.data.detail);
        }
    }

    const onToastRemove = () => {
        history.goBack();
    };

    return (
        <div>
            <Toast ref={toast} onRemove={onToastRemove}/>
            <Panel header="Informations" className="p-mb-2" toggleable>
                <div className="p-fluid p-formgrid p-grid">
                    <div className="p-field p-col-12 p-md-6">
                        <label htmlFor="nom">Nom</label>
                        <InputText id="nom" type="text" value={nom} onChange={onNomChange}/>
                    </div>
                    <div className="p-field p-col-12 p-md-6">
                        <label htmlFor="prenom">Prénom</label>
                        <InputText id="prenom" type="text" value={prenom}
                                   onChange={onPrenomChange}/>
                    </div>
                    <div className="p-field p-col-12 p-md-6">
                        <label htmlFor="adresse">Adresse</label>
                        <InputText id="adresse" type="text" value={adresse}
                                   onChange={onAdresseChange}/>
                    </div>
                    <div className="p-field p-col-12 p-md-6">
                        <label htmlFor="complementAdresse">Complément d'adresse</label>
                        <InputText id="complementAdresse" type="text" value={complementAdresse}
                                   onChange={onComplementAdresseChange}/>
                    </div>
                    <div className="p-field p-col-12 p-md-6">
                        <label htmlFor="codePostal">Code postal</label>
                        <InputText id="codePostal" type="text" value={codePostal}
                                   onChange={onCodePostalChange}/>
                        {!codePostalValide &&
                        <small id="codePostalErreur" className="p-invalid p-d-block">Veuillez renseigner un code postal
                            valide.</small>}
                    </div>
                    <div className="p-field p-col-12 p-md-6">
                        <label htmlFor="ville">Ville</label>
                        <InputText id="ville" type="text" value={ville}
                                   onChange={onVilleChange}/>
                    </div>
                    <div className="p-field p-col-12 p-md-6">
                        <label htmlFor="telephone">Téléphone</label>
                        <InputText id="telephone" type="text" value={telephone}
                                   onChange={onTelephoneChange}/>
                    </div>
                    <div className="p-field p-col-12 p-md-6">
                        <label htmlFor="mail">Mail</label>
                        <InputText id="mail" type="text" value={mail}
                                   onChange={onMailChange}/>
                        {!adresseMailValide &&
                        <small id="adresseMailValide" className="p-invalid p-d-block">Veuillez renseigner une adresse
                            email valide.</small>}
                    </div>
                </div>
            </Panel>
            <div className="p-d-flex p-jc-end">
                <Button label="Retour" className="p-mr-2" onClick={() => history.goBack()}/>
                <Button label="Valider" disabled={!formulaireValide} onClick={enregistrerClient}/>
            </div>
        </div>
    )
}
export default ClientEditComponent;
