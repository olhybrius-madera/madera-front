import {Panel} from "primereact/panel";
import {DataTable} from "primereact/datatable";
import {Column} from "primereact/column";
import React, {useEffect, useState} from "react";
import {Button} from "primereact/button";
import {getAllModules} from "../services/ConfigurationService";
import {useKeycloak} from "@react-keycloak/web";

const GammeConsultComponent = (props) => {
    const {gamme} = props.location.state;
    const {keycloak} = useKeycloak();
    const [modules, setModules] = useState([]);

    useEffect(() => {
            (async () => {
                const data = await getAllModules(keycloak.token);
                setModules(data.filter(module => module.gamme === gamme.uuid));
            })();
        }, []
    )
    const formatMontant = (value) => {
        return `${String(value).replace('.', ',')} €`;
    };

    const prixUnitaireBodyTemplate = (rowData) => {
        return formatMontant(rowData.prix);
    }

    return (
        <div>
            <Panel header="Informations" className="p-mb-2" toggleable>
                <p>Libellé : {gamme.libelle}</p>
                <p>Finition extérieure : {gamme.finition_exterieure}</p>
                <p>Type d'isolant : {gamme.type_isolant}</p>
                <p>Type de couverture : {gamme.type_couverture}</p>
            </Panel>
            <Panel header="Modules" className="p-mb-2" toggleable>
                <DataTable value={modules} removableSort>
                    <Column field="libelle" header="Libellé" sortable/>
                    <Column field="unite" header="Unite" sortable/>
                    <Column field="prix" header="Prix unitaire" body={prixUnitaireBodyTemplate} sortable/>
                </DataTable>
            </Panel>
            <div className="p-d-flex p-jc-end">
                <Button label="Retour" onClick={() => props.history.goBack()}/>
            </div>
        </div>);
}
export default GammeConsultComponent;
