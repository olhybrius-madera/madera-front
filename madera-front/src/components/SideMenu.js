import React from 'react';
import {useHistory} from "react-router-dom";
import {Menu} from 'primereact/menu';
import useAuthorization from "../hooks/useAuthorization";
import {useKeycloak} from "@react-keycloak/web";

const SideMenu = () => {
    const history = useHistory();
    const {keycloak} = useKeycloak()
    const items = [
        useAuthorization(['commercial', 'client']) && {
            label: 'Devis',
            items: [
                {
                    label: 'Liste',
                    icon: 'pi pi-copy',
                    command: () => {
                        history.push("/devis")
                    }
                },
                keycloak.hasRealmRole('commercial') && {
                    label: 'Nouveau',
                    icon: 'pi pi-file',
                    command: () => {
                        history.push("/devis/nouveau");
                    }
                }
            ]
        },
        useAuthorization(['bureau_etude']) && {
            label: 'Configuration',
            items: [
                {
                    label: 'Gammes',
                    icon: 'pi pi-cog',
                    command: () => {
                        history.push("/gammes")
                    }
                },
                {
                    label: 'Modules',
                    icon: 'pi pi-cog',
                    command: () => {
                        history.push("/modules")
                    }
                },
                {
                    label: 'Composants',
                    icon: 'pi pi-cog',
                    command: () => {
                        history.push("/composants")
                    }
                }

            ]
        },
        useAuthorization(['commercial']) && {
            label: 'Clients',
            items: [
                {
                    label: 'Liste',
                    icon: 'pi pi-users',
                    command: () => {
                        history.push("/clients")
                    }
                },
                {
                    label: 'Nouveau',
                    icon: 'pi pi-user-plus',
                    command: () => {
                        history.push("/client/nouveau")
                    }
                }
            ]
        }
    ];
    return (
        <Menu model={items}/>
    )
}
export default SideMenu;
