import {useKeycloak} from "@react-keycloak/web";
import {Link, useHistory} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {getAllClients } from "../services/DevisService";
import {Button} from "primereact/button";
import {DataTable} from "primereact/datatable";
import {Column} from "primereact/column";

const ClientTableComponent = () => {
    const {keycloak} = useKeycloak();
    const history = useHistory();
    const [clients, setClients] = useState([]);

    useEffect(() => {
            (async () => {
                const data = await getAllClients(keycloak.token);
                setClients(data);
            })();
        }, []
    )

    const actionBodyTemplate = (rowData) => {
        return (
            <Button type="button" icon="pi pi-search" className="p-button-rounded"
                    onClick={() => history.push('/client/consult', {client: rowData})}/>
        );
    }

    return (
        <div>
            <div className="p-d-flex">
                <Link to="/client/nouveau" className="p-mb-2 p-ml-auto">
                    <Button label="Nouveau client"/>
                </Link>
            </div>
            <div className="p-grid p-justify-around">
                <div className="p-col">
                    <DataTable value={clients} className="p-datatable-striped" autoLayout={true} removableSort>
                        <Column field="nom" header="Nom" sortable/>
                        <Column field="prenom" header="Prénom" sortable/>
                        <Column field="adresse" header="Adresse" sortable/>
                        <Column field="complement_adresse" header="Complément d'adresse" sortable/>
                        <Column field="code_postal" header="Code Postal" sortable/>
                        <Column field="ville" header="Ville" sortable/>
                        <Column field="telephone" header="Téléphone" sortable/>
                        <Column field="mail" header="Mail" sortable/>
                        <Column body={actionBodyTemplate}/>
                    </DataTable>
                </div>
            </div>
        </div>
    );
}
export default ClientTableComponent;
