import {Panel} from "primereact/panel";
import {Button} from "primereact/button";

const ComposantConsultComponent = (props) => {
    const {composant} = props.location.state;

    const formatMontant = (value) => {
        return `${String(value).replace('.', ',')} €`;
    };

    return (
        <div>
            <Panel header="Informations" className="p-mb-2" toggleable>
                <p>Libellé : {composant.libelle}</p>
                <p>Prix unitaire : {formatMontant(composant.prix)}</p>
                <p>Unité : {composant.unite}</p>
            </Panel>
            <div className="p-d-flex p-jc-end">
                <Button label="Retour" onClick={() => props.history.goBack()}/>
            </div>
        </div>);
}
export default ComposantConsultComponent;
