import React from 'react';
import {Menubar} from 'primereact/menubar';
import {Button} from 'primereact/button';
import {useKeycloak} from "@react-keycloak/web";
import logo from "../kermadera2.png"
import {Link} from "react-router-dom";

const Header = () => {
    const {keycloak} = useKeycloak();
    const start = (<Link to={"/"}><img src={logo} height="40" className="p-mr-2"/></Link>);
    const end = <Button label="Déconnexion" icon="pi pi-sign-out" onClick={() => keycloak.logout()}/>;

    return (
        <div>
            <div className="card">
                <Menubar start={start} end={end}/>
            </div>
        </div>
    );
}
export default Header;
