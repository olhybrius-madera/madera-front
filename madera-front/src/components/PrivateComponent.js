import useAuthorization from "../hooks/useAuthorization";

const PrivateComponent = ({roles, children}) => {
    const hasAccess = useAuthorization(roles);

    return hasAccess && children;
}
export default PrivateComponent;
