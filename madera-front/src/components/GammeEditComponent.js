import {Panel} from "primereact/panel";
import {Button} from "primereact/button";
import {InputText} from "primereact/inputtext";
import React, {useEffect, useState} from "react";
import {useHistory} from "react-router-dom";
import {useKeycloak} from "@react-keycloak/web";
import {Dropdown} from "primereact/dropdown";
import {envoyerGamme} from "../services/ConfigurationService";

const GammeEditComponent = () => {
    const history = useHistory();
    const {keycloak} = useKeycloak();
    const [libelle, setLibelle] = useState('');
    const [finitionExterieure, setFinitionExterieure] = useState('');
    const [typeIsolant, setTypeIsolant] = useState(null);
    const [typeCouverture, setTypeCouverture] = useState('');
    const [typesIsolantSelectItems, setTypesIsolantSelectItems] = useState(null);
    const formulaireValide = libelle && finitionExterieure && typeIsolant && typeCouverture;

    const typesIsolant = [
        'SYNTHETIQUE',
        'NATUREL',
        'BIOLOGIQUE'
    ]

    useEffect(() => {
            (async () => {
                const selectItems = typesIsolant.map(type => ({
                    label: type,
                    value: type
                }));
                setTypesIsolantSelectItems(selectItems);
            })();
        }, []
    )

    const onLibelleChange = (e) => {
        setLibelle(e.target.value);
    }

    const onTypeIsolantChange = (e) => {
        setTypeIsolant(e.value);
    }

    const onFinitionExterieureChange = (e) => {
        setFinitionExterieure(e.target.value);
    }

    const onTypeCouvertureChange = (e) => {
        setTypeCouverture(e.target.value);
    }

    const enregistrerGamme = async () => {
        const payload = {
            libelle: libelle,
            finition_exterieure: finitionExterieure,
            type_isolant: typeIsolant,
            type_couverture: typeCouverture,
        };
        try {
            await envoyerGamme(keycloak.token, payload);
            history.goBack()
        } catch (error) {

        }
    }

    return (
        <div>
            <Panel header="Informations" className="p-mb-2" toggleable>
                <div className="p-fluid p-formgrid p-grid">
                    <div className="p-field p-col-12 p-md-6">
                        <label htmlFor="libelle">Libellé</label>
                        <InputText id="libelle" type="text" value={libelle} onChange={onLibelleChange}/>
                    </div>
                    <div className="p-field p-col-12 p-md-3">
                        <label htmlFor="typeIsolant">Type d'isolant</label>
                        <Dropdown inputId="typeIsolant" value={typeIsolant} options={typesIsolantSelectItems}
                                  onChange={onTypeIsolantChange} placeholder="Type d'isolant"/>
                    </div>
                    <div className="p-field p-col-12 p-md-6">
                        <label htmlFor="finitonExterieure">Finition extérieure</label>
                        <InputText id="finitionExterieure" type="text" value={finitionExterieure}
                                   onChange={onFinitionExterieureChange}/>
                    </div>
                    <div className="p-field p-col-12 p-md-6">
                        <label htmlFor="typeCouverture">Type de couverture</label>
                        <InputText id="typeCouverture" type="text" value={typeCouverture}
                                   onChange={onTypeCouvertureChange}/>
                    </div>
                </div>
            </Panel>
            <div className="p-d-flex p-jc-end">
                <Button label="Retour" className="p-mr-2" onClick={() => history.goBack()}/>
                <Button label="Valider" disabled={!formulaireValide} onClick={enregistrerGamme}/>
            </div>
        </div>
    )
}
export default GammeEditComponent;
