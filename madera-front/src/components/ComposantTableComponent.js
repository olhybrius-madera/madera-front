import {useKeycloak} from "@react-keycloak/web";
import {Link, useHistory} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {Button} from "primereact/button";
import {DataTable} from "primereact/datatable";
import {Column} from "primereact/column";
import {getAllComposants} from "../services/ConfigurationService";

const ComposantTableComponent = () => {
    const {keycloak} = useKeycloak();
    const history = useHistory();
    const [composants, setComposants] = useState([]);


    useEffect(() => {
            (async () => {
                const data = await getAllComposants(keycloak.token);
                setComposants(data);
            })();
        }, []
    )

    const formatMontant = (value) => {
        return `${String(value).replace('.', ',')} €`;
    };

    const montantBodyTemplate = (rowData) => {
        const {prix} = rowData;
        return formatMontant(prix);
    };

    const actionBodyTemplate = (rowData) => {
        return (
            <Button type="button" icon="pi pi-search" className="p-button-rounded"
                    onClick={() => history.push('/composant/consult', {composant: rowData})}/>
        );
    }

    return (
            <div className="p-grid p-justify-around">
                <div className="p-col">
                    <DataTable value={composants} className="p-datatable-striped" autoLayout={true} removableSort>
                        <Column field="libelle" header="Libellé" sortable/>
                        <Column field="unite" header="Unité" sortable/>
                        <Column field="prix" header="Prix unitaire" body={montantBodyTemplate} sortable/>
                        <Column body={actionBodyTemplate}/>
                    </DataTable>
                </div>
            </div>
    );
}
export default ComposantTableComponent;
