import {Panel} from "primereact/panel";
import {Dropdown} from "primereact/dropdown";
import {Button} from "primereact/button";
import {DataTable} from "primereact/datatable";
import {Column} from "primereact/column";
import React, {useEffect, useRef, useState} from "react";
import {getAllClients, getAllGammes, soumettreDevis} from "../services/DevisService";
import {useHistory} from "react-router-dom";
import {InputText} from "primereact/inputtext";
import {InputNumber} from 'primereact/inputnumber';
import {useKeycloak} from "@react-keycloak/web";
import {Toast} from "primereact/toast";
import {getAllModules} from "../services/ConfigurationService";


const DevisComponent = () => {
    const history = useHistory();
    const {keycloak} = useKeycloak();
    const [referenceProjet, setReferenceProjet] = useState('');
    const [nomProjet, setNomProjet] = useState('');
    const [clients, setClients] = useState([]);
    const [gammes, setGammes] = useState([]);
    const [modules, setModules] = useState([]);
    const [modulesDisponibles, setModulesDisponibles] = useState([]);
    const [lignesModules, setLignesModules] = useState([]);
    const [clientSelectItems, setClientSelectItems] = useState([]);
    const [selectedClientUuid, setSelectedClientUuid] = useState(null);
    const [gammeSelectItems, setGammeSelectItems] = useState([]);
    const [selectedGammeUuid, setSelectedGammeUuid] = useState(null);
    const [moduleSelectItems, setModuleSelectItems] = useState(null);
    const [total, setTotal] = useState(0.0);
    const toast = useRef(null);
    const ref = useRef(null);

    useEffect(() => {
            (async () => {
                const data = await getAllClients(keycloak.token);
                setClients(data);
                const selectItems = data.map(client => ({
                    label: `${client.nom} ${client.prenom}`,
                    value: client.uuid
                }));
                setClientSelectItems(selectItems);
            })();
        }, []
    )

    useEffect(() => {
            (async () => {
                const data = await getAllGammes(keycloak.token);
                setGammes(data);
                const selectItems = data.map(gamme => ({
                    label: gamme.libelle,
                    value: gamme.uuid
                }));
                setGammeSelectItems(selectItems);
            })();
        }, []
    )

    useEffect(() => {
            (async () => {
                const data = await getAllModules(keycloak.token);
                setModules(data);
            })();
        }, []
    )

    useEffect(() => {
            (async () => {
                const data = modules.filter(module => module.gamme === selectedGammeUuid)
                setModulesDisponibles(data);
                const selectItems = data.map(module => ({
                    label: module.libelle,
                    value: module.uuid
                }));
                setModuleSelectItems(selectItems);
            })();
        }, [selectedGammeUuid]
    )

    useEffect(() => {
        setModuleSelectItems(modulesDisponibles?.map(module => ({
                label: module.libelle,
                value: module.uuid
            })).filter(module => !lignesModules.some(ligne => ligne.libelle === module.value))
        )
    }, [lignesModules])

    useEffect(() => {
        const sommeLignes = lignesModules.reduce((acc, curr) => {
            return ((acc * 100) + ((curr.prix * 100) * curr.quantite)) / 100;
        }, 0.0);
        setTotal(sommeLignes)
    }, [lignesModules]);

    const getSelectedClient = () => clients.find(client => client.uuid === selectedClientUuid);
    const getSelectedGamme = () => gammes.find(gamme => gamme.uuid === selectedGammeUuid);

    const ajouterLigne = () => {
        const nouvelleLigne = {quantite: 1};
        setLignesModules([...lignesModules, nouvelleLigne]);
    };

    const formatMontant = (value) => {
        return `${String(value).replace('.', ',')} €`;
    };

    const prixUnitaireBodyTemplate = (rowData) => {
        const {libelle} = rowData;
        rowData.prix = modules.find(module => module.uuid === libelle)?.prix ?? 0.0;
        return formatMontant(rowData.prix);
    }

    const prixLigneBodyTemplate = (rowData) => {
        rowData.prixLigne = ((rowData.prix * 100) * rowData.quantite) / 100;
        return (
            <span>{formatMontant(rowData.prixLigne)}</span>
        );
    }

    const onEditorValueChange = (props, value) => {
        let updatedProducts = [...props.value];
        updatedProducts[props.rowIndex][props.field] = value;
        setLignesModules(updatedProducts);
        ref.current.closeEditingCell();
    }

    const libelleEditor = (props) => {
        return (
            <Dropdown value={props.rowData['libelle']} options={moduleSelectItems} optionLabel="label"
                      optionValue="value"
                      onChange={(e) => {
                          onEditorValueChange(props, e.value);

                      }} style={{width: '100%'}}
                      placeholder="Choisir un module"
            />
        );
    }

    const quantiteEditor = (props, field) => {
        return <InputNumber value={props.rowData[field]}
                            onValueChange={(e) => onEditorValueChange(props, e.target.value)}/>;
    }

    const moduleBodyTemplate = (rowData) => {
        return modulesDisponibles.find(module => module.uuid === rowData.libelle)?.libelle;
    }

    const deleteLigne = (rowData) => {
        setLignesModules(lignesModules.filter(ligne => ligne.libelle !== rowData.libelle));
    }

    const actionBodyTemplate = (rowData) => {
        return (
            <React.Fragment>
                <Button icon="pi pi-trash" className="p-button-rounded p-button-warning"
                        onClick={() => deleteLigne(rowData)}/>
            </React.Fragment>
        );
    }

    const formulaireValide = () => {
        return referenceProjet && nomProjet && selectedClientUuid && selectedGammeUuid
            && lignesModules.filter(ligne => ligne.libelle).length > 0;
    }

    const showToast = (severityValue, summaryValue, detailValue) => {
        toast.current.show({
            severity: severityValue, summary: summaryValue, detail: detailValue
        });
    }

    const enregistrerDevis = async () => {
        const payload = {
            commercial: keycloak.tokenParsed.sub,
            reference_projet: referenceProjet,
            nom_projet: nomProjet,
            client: selectedClientUuid,
            gamme: selectedGammeUuid,
            modules: lignesModules.map(ligne => ({uuid: ligne.libelle, quantite: ligne.quantite}))
        };
        try {
            await soumettreDevis(keycloak.token, payload)
            showToast('success', `Soumission du devis effectuée !`,
                '');
        } catch (error) {
            showToast('error', 'Une erreur est survenue', error.response.data.detail);
        }
    }

    const onToastRemove = () => {
        history.goBack();
    };

    return (
        <div>
            <Toast ref={toast} onRemove={onToastRemove}/>
            <div className="p-grid nested-grid">
                <div className="p-col p-col-align-stretch">
                    <Panel header="Projet" className="p-mb-2" toggleable>
                        <p> Référence Projet :</p><InputText value={referenceProjet}
                                                             onChange={(e) => setReferenceProjet(e.target.value)}/>
                        <p> Nom Projet :</p><InputText value={nomProjet}
                                                       onChange={(e) => setNomProjet(e.target.value)}/>
                    </Panel>
                </div>
                <div className="p-col">
                    <Panel header="Client" className="p-mb-2" toggleable>
                        <Dropdown value={selectedClientUuid} options={clientSelectItems}
                                  onChange={(e) => setSelectedClientUuid(e.value)}
                                  placeholder="Choisir un client"/>
                        <p> Nom: {getSelectedClient()?.nom} </p>
                        <p> Prénom : {getSelectedClient()?.prenom} </p>
                        <p> Adresse : {getSelectedClient()?.adresse}</p>
                        <p> Ville
                            : {`${getSelectedClient()?.code_postal ?? ''} ${getSelectedClient()?.ville ?? ''}`}</p>
                    </Panel>
                </div>
                <div className="p-col">
                    <Panel header="Gamme" className="p-mb-2" toggleable>
                        <Dropdown value={selectedGammeUuid} options={gammeSelectItems}
                                  onChange={(e) => setSelectedGammeUuid(e.value)}
                                  placeholder="Choisir une gamme"/>
                        <p>Libellé : {getSelectedGamme()?.libelle}</p>
                        <p>Finition extérieure : {getSelectedGamme()?.finition_exterieure}</p>
                        <p>Type d'isolant : {getSelectedGamme()?.type_isolant}</p>
                        <p>Type de couverture : {getSelectedGamme()?.type_couverture}</p>
                    </Panel>
                </div>
            </div>
            <Panel header="Modules" className="p-mb-2" toggleable>
                <div className="p-d-flex">
                    <Button disabled={!selectedGammeUuid} label="Ajouter un module" className="p-mb-2 p-ml-auto"
                            onClick={() => ajouterLigne()}/>
                </div>
                <DataTable ref={ref} value={lignesModules} editMode="cell" className="editable-cells-table">
                    <Column field="libelle" header="Libellé" body={moduleBodyTemplate}
                            editor={(props) => libelleEditor(props)}/>
                    <Column field="prix" header="Prix unitaire" body={prixUnitaireBodyTemplate}/>
                    <Column field="quantite" header="Quantité"
                            editor={(props) => quantiteEditor(props, 'quantite')}/>
                    <Column header="Prix ligne" body={prixLigneBodyTemplate}/>
                    <Column body={actionBodyTemplate}/>
                </DataTable>
            </Panel>
            <div className="p-grid p-justify-end">
                <div className="p-col-4">
                    <Panel header="Total" className="p-mb-2" toggleable>
                        <div className="p-grid p-jc-between">
                            <div className="p-col">Total :</div>
                            <span className="p-col" style={{textAlign: 'right'}}>{formatMontant(total)}</span>
                        </div>
                    </Panel>
                </div>
            </div>
            <div className="p-d-flex p-jc-end">
                <Button label="Annuler" className="p-mr-2" onClick={() => history.goBack()}/>
                <Button label="Valider"
                        disabled={!formulaireValide()}
                        onClick={enregistrerDevis}/>
            </div>
        </div>
    );
}
export default DevisComponent;
