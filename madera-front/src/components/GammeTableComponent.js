import {useKeycloak} from "@react-keycloak/web";
import {Link, useHistory} from "react-router-dom";
import React, {useEffect, useState} from "react";
import {getAllGammes} from "../services/DevisService";
import {Button} from "primereact/button";
import {DataTable} from "primereact/datatable";
import {Column} from "primereact/column";

const GammeTable = () => {
    const {keycloak} = useKeycloak();
    const history = useHistory();
    const [gammes, setGammes] = useState([]);

    useEffect(() => {
            (async () => {
                const data = await getAllGammes(keycloak.token);
                setGammes(data);
            })();
        }, []
    )

    const actionBodyTemplate = (rowData) => {
        return (
            <Button type="button" icon="pi pi-search" className="p-button-rounded" onClick={() => history.push('/gamme/consult', {gamme: rowData})}/>
        );
    }

    return (
        <div>
            <div className="p-d-flex">
                <Link to="/gamme/nouveau" className="p-mb-2 p-ml-auto">
                    <Button label="Nouvelle Gamme"/>
                </Link>
            </div>
            <div className="p-grid p-justify-around">
                <div className="p-col">
                    <DataTable value={gammes} className="p-datatable-striped" autoLayout={true} removableSort>
                        <Column field="libelle" header="Libellé" sortable/>
                        <Column field="finition_exterieure" header="Finition extérieure" sortable/>
                        <Column field="type_isolant" header="Type d'isolant" sortable/>
                        <Column field="type_couverture" header="Type de couverture" sortable/>
                        <Column body={actionBodyTemplate} />
                    </DataTable>
                </div>
            </div>
        </div>
    );
}
export default GammeTable;
