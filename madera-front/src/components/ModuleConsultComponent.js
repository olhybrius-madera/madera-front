import React, {useEffect, useState} from "react";
import {Panel} from "primereact/panel";
import {DataTable} from "primereact/datatable";
import {Column} from "primereact/column";
import {Button} from "primereact/button";
import {getAllGammes} from "../services/DevisService";
import {useKeycloak} from "@react-keycloak/web";
import {getAllComposants} from "../services/ConfigurationService";

const ModuleConsultComponent = (props) => {
    const {module} = props.location.state;
    const {keycloak} = useKeycloak();
    const [composants, setComposants] = useState([]);
    const [gamme, setGamme] = useState(null);

    useEffect(() => {
            (async () => {
                const data = await getAllGammes(keycloak.token);
                const gammeModule = data.find(g => g.uuid === module.gamme);
                setGamme(gammeModule);
            })();
        }, []
    )

    useEffect(() => {
            (async () => {
                const data = await getAllComposants(keycloak.token);
                setComposants(data.filter(composant => module.composants.map(c => c.uuid).includes(composant.uuid)));
            })();
        }, []
    )

    const formatMontant = (value) => {
        return `${String(value).replace('.', ',')} €`;
    };

    const prixUnitaireBodyTemplate = (rowData) => {
        return formatMontant(rowData.prix);
    }

    const quantiteBodyTemplate = (rowData) => {
        const {uuid} = rowData;
        return module.composants.find(composant => composant.uuid === uuid).quantite;
    }

    return (
        <div>
            <Panel header="Informations" className="p-mb-2" toggleable>
                <p>Libellé : {module.libelle}</p>
                <p>Prix unitaire : {formatMontant(module.prix)}</p>
                <p>Unité : {module.unite}</p>
                <p>Gamme : {gamme?.libelle}</p>
            </Panel>
            <Panel header="Composants" className="p-mb-2" toggleable>
                <DataTable value={composants} removableSort>
                    <Column field="libelle" header="Libellé" sortable/>
                    <Column field="unite" header="Unite" sortable/>
                    <Column field="prix" header="Prix unitaire" body={prixUnitaireBodyTemplate} sortable/>
                    <Column field="quantite" header="Quantité" body={quantiteBodyTemplate} sortable/>
                </DataTable>
            </Panel>
            <div className="p-d-flex p-jc-end">
                <Button label="Retour" onClick={() => props.history.goBack()}/>
            </div>
        </div>);
}
export default ModuleConsultComponent;
