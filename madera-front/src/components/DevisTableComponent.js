import {DataTable} from 'primereact/datatable';
import React, {useEffect, useState} from 'react';
import {Column} from 'primereact/column';
import dayjs from 'dayjs'
import {Button} from "primereact/button";
import {Link, useHistory} from "react-router-dom";
import {getAllClients, getAllDevis} from "../services/DevisService";
import {useKeycloak} from "@react-keycloak/web";
import PrivateComponent from "./PrivateComponent";

const DevisTable = () => {
    const {keycloak} = useKeycloak();
    const history = useHistory();
    const [listeDevis, setListeDevis] = useState([]);
    const [clients, setClients] = useState([]);

    useEffect(() => {
            (async () => {
                const data = await getAllDevis(keycloak.token);
                setListeDevis(data);
            })();
        }, []
    )

    useEffect(() => {
            if (keycloak.hasRealmRole('commercial')) {
                (async () => {
                    const data = await getAllClients(keycloak.token);
                    setClients(data);
                })();
            }
        }, []
    )

    const formatDate = (value) => {
        return dayjs(value).format('DD/MM/YYYY HH:mm:ss');
    };

    const dateBodyTemplate = (rowData) => {
        const {date_emission} = rowData;
        if (!date_emission) {
            return 'Devis non-accepté';
        }
        return formatDate(date_emission);
    };

    const formatMontant = (value) => {
        return `${String(value).replace('.', ',')} €`;
    };

    const montantBodyTemplate = (rowData) => {
        const {prix} = rowData;
        return formatMontant(prix);
    };

    const clientBodyTemplate = (rowData) => {
        const {client} = rowData;
        const clientInfos = clients.find(c => c.uuid === client);
        return `${clientInfos?.prenom ?? ''} ${clientInfos?.nom ?? ''}`
    }

    const gammeBodyTemplate = (rowData) => {
        const {gamme} = rowData;
        return gamme.libelle;
    }

    const actionBodyTemplate = (rowData) => {
        return (
            <Button type="button" icon="pi pi-search" className="p-button-rounded"
                    onClick={() => history.push('/devis/consult', {devis: rowData})}/>
        );
    }

    return (
        <div>
            <PrivateComponent roles={['commercial']}>
                <div className="p-d-flex">
                    <Link to="/devis/nouveau" className="p-mb-2 p-ml-auto">
                        <Button label="Nouveau Devis"/>
                    </Link>
                </div>
            </PrivateComponent>
            <div className="p-grid p-justify-around">
                <div className="p-col">
                    <DataTable value={listeDevis} className="p-datatable-striped" autoLayout={true} removableSort>
                        <Column field="reference_projet" header="Référence Projet" sortable/>
                        <Column field="nom_projet" header="Nom Projet" sortable/>
                        {keycloak.hasRealmRole('commercial') &&
                        <Column field="client" header="Client" body={clientBodyTemplate} sortable/>}
                        <Column field="gamme" header="Gamme" sortable body={gammeBodyTemplate}/>
                        <Column field="prix" header="prix" body={montantBodyTemplate} sortable/>
                        <Column field="date_emission" header="Date d'émission" body={dateBodyTemplate} sortable/>
                        <Column field="etat" header="Etat" sortable/>
                        <Column body={actionBodyTemplate}/>
                    </DataTable>
                </div>
            </div>
        </div>
    )
        ;
}
export default DevisTable;
