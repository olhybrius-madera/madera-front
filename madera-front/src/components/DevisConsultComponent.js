import React, {useEffect, useRef, useState} from "react";
import {Panel} from "primereact/panel";
import {Button} from "primereact/button";
import {DataTable} from "primereact/datatable";
import {Column} from "primereact/column";
import {Toast} from 'primereact/toast';
import {executerAction, getAllClients, getAllGammes, telecharcherPdf} from "../services/DevisService";
import {useKeycloak} from "@react-keycloak/web";
import PrivateComponent from "./PrivateComponent";

const DevisConsultComponent = (props) => {
    const {devis} = props.location.state;
    const {keycloak} = useKeycloak();
    const [clients, setClients] = useState([]);
    const [gammes, setGammes] = useState([]);
    const toast = useRef(null);

    useEffect(() => {
            if (keycloak.hasRealmRole('commercial')) {
                (async () => {
                    const data = await getAllClients(keycloak.token);
                    setClients(data);
                })();
            }
        }, []
    )

    useEffect(() => {
            (async () => {
                const data = await getAllGammes(keycloak.token);
                setGammes(data);
            })();
        }, []
    )

    const getSelectedClient = () => clients.find(client => client.uuid === devis.client);
    const getSelectedGamme = () => gammes.find(gamme => gamme.uuid === devis.gamme);

    const formatMontant = (value) => {
        return `${String(value).replace('.', ',')} €`;
    };

    const prixUnitaireBodyTemplate = (rowData) => {
        return formatMontant(rowData.prix);
    }

    const prixLigneBodyTemplate = (rowData) => {
        rowData.prixLigne = ((rowData.prix * 100) * rowData.quantite) / 100;
        return (
            <span>{formatMontant(rowData.prixLigne)}</span>
        );
    }

    const showToast = (severityValue, summaryValue, detailValue) => {
        toast.current.show({
            severity: severityValue, summary: summaryValue, detail: detailValue
        });
    }

    const executer = async (action) => {
        try {
            await executerAction(keycloak.token, action, devis.uuid);
            showToast('success', `Devis ${action === 'accepter' ? 'accepté' : 'refusé'}`,
                '');
        } catch (error) {
            showToast('error', 'Une erreur est survenue', error.response.data.detail);
        }
    }

    const onToastRemove = () => {
        props.history.goBack();
    };

    const telecharger = async () => {
        const file = await telecharcherPdf(keycloak.token, devis.uuid)
        const fileURL = URL.createObjectURL(file);
        const link = document.createElement('a');
        link.setAttribute('download', 'devis.pdf')
        link.href = fileURL;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }

    return (
        <div>
            <Toast ref={toast} onRemove={onToastRemove}/>
            <div className="p-grid nested-grid">
                <div className="p-col p-col-align-stretch">
                    <Panel header="Projet" className="p-mb-2" toggleable>
                        <p> Référence Projet : {devis.reference_projet}</p>
                        <p> Nom Projet : {devis.nom_projet}</p>
                        <p> Etat : {devis.etat}</p>
                    </Panel>
                </div>
                <PrivateComponent roles={['commercial']}>
                    <div className="p-col">
                        <Panel header="Client" className="p-mb-2" toggleable>
                            <p> Nom: {getSelectedClient()?.nom} </p>
                            <p> Prénom : {getSelectedClient()?.prenom} </p>
                            <p> Adresse : {getSelectedClient()?.adresse}</p>
                            <p> Ville
                                : {`${getSelectedClient()?.code_postal ?? ''} ${getSelectedClient()?.ville ?? ''}`}</p>
                        </Panel>
                    </div>
                </PrivateComponent>
                <div className="p-col">
                    <Panel header="Gamme" className="p-mb-2" toggleable>
                        <p>Libellé : {devis.gamme.libelle}</p>
                        <p>Finition extérieure : {devis.gamme.finition_exterieure}</p>
                        <p>Type d'isolant : {devis.gamme.type_isolant}</p>
                        <p>Type de couverture : {devis.gamme.type_couverture}</p>
                    </Panel>
                </div>
            </div>
            <Panel header="Modules" className="p-mb-2" toggleable>
                <DataTable value={devis.modules}>
                    <Column field="libelle" header="Libellé"/>
                    <Column field="prix" header="Prix unitaire" body={prixUnitaireBodyTemplate}/>
                    <Column field="quantite" header="Quantité"/>
                    <Column header="Prix ligne" body={prixLigneBodyTemplate}/>
                </DataTable>
            </Panel>
            <div className="p-grid p-justify-end">
                <div className="p-col-4">
                    <Panel header="Total" className="p-mb-2" toggleable>
                        <div className="p-grid p-jc-between">
                            <div className="p-col">Total :</div>
                            <span className="p-col" style={{textAlign: 'right'}}>{formatMontant(devis.prix)}</span>
                        </div>
                    </Panel>
                </div>
            </div>
            <div className="p-d-flex p-jc-end">
                <Button label="Retour" className="p-mr-2" onClick={() => props.history.goBack()}/>
                <Button label="Télécharger" className="p-mr-2" onClick={telecharger}/>
                <PrivateComponent roles={['client']}>
                    <Button label="Refuser" className="p-mr-2" onClick={async () => executer('refuser')}/>
                </PrivateComponent>
                <PrivateComponent roles={['client']}>
                    <Button label="Accepter" className="p-mr-2" onClick={async () => executer('accepter')}/>
                </PrivateComponent>
            </div>
        </div>
    );
}
export default DevisConsultComponent;
