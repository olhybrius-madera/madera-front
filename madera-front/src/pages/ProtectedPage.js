import React from 'react';

const ProtectedPage = () => {

    return (
        <div>
            <h1>Page protégée</h1>
            <strong>Bravo, vous avez le rôle requis pour accéder à cette page !</strong>
        </div>
    )
}
export default ProtectedPage
