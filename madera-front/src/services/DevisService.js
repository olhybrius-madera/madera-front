import axios from 'axios';

export const getAllClients = async (token) => {
    const response = await axios.get(
        `http://localhost:8002/clients`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }
    );
    return response.data;
}

export const getAllGammes = async (token) => {
    const response = await axios.get(
        `http://localhost:8003/gammes`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }
    );
    return response.data;
}

export const getAllDevis = async (token) => {

    const response = await axios.get(
        `http://localhost:8001/devis`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }
    );
    return response.data;
};

export const executerAction = async (token, action, uuidDevis) => {

    const response = await axios.put(`http://localhost:8001/devis/${uuidDevis}/${action}/`, {}, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export const soumettreDevis = async (token, payload) => {

    const response = await axios.post(`http://localhost:8001/devis/`, payload, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export const telecharcherPdf = async (token, uuid) => {
    const response = await axios.get(
        `http://localhost:8001/devis/${uuid}/telecharger/`, {
            headers: {
                Authorization: `Bearer ${token}`
            },
           responseType: 'blob'
        }
    );
    const fichier = new Blob(
        [response.data],
        {type: 'application/pdf'});
    return fichier;
}
