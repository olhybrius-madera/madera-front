import axios from "axios";

export const getAllModules = async (token) => {
    const response = await axios.get(
        `http://localhost:8003/modules`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }
    );
    return response.data;
}

export const getAllComposants = async (token) => {
    const response = await axios.get(
        `http://localhost:8003/composants`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }
    );
    return response.data;
}

export const importerModule = async (token, fichier) => {
    const formData = new FormData();
    formData.append('file', fichier);
    const response = await axios.post(`http://localhost:8003/upload/`, formData, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
    return response.data;
}

export const envoyerGamme = async (token, payload) => {
    const response = await axios.post(`http://localhost:8003/gammes/`, payload, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}
