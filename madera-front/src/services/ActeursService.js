import axios from "axios";

export const soumettreFicheClient = async (token, payload) => {
    const response = await axios.post(`http://localhost:8002/clients/`, payload, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
};
