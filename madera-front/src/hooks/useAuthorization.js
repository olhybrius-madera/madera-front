import {useEffect, useState} from "react";
import {useKeycloak} from "@react-keycloak/web";

const useAuthorization = (allowedRoles) => {
    const {keycloak} = useKeycloak();
    const [hasAccess, setHasAccess] = useState(false);

    useEffect(() => {
        const checkAuthorization = () => {
            if (!allowedRoles) {
                setHasAccess(keycloak.authenticated);
            } else {
                setHasAccess(allowedRoles.some(r => {
                    const realm = keycloak.hasRealmRole(r);
                    const resource = keycloak.hasResourceRole(r);
                    return realm || resource;
                }));
            }
        }
        checkAuthorization();
    }, [keycloak, allowedRoles]);

    return hasAccess;

}
export default useAuthorization;
