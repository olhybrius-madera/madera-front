#!/bin/bash
set -euo pipefail

source .env

sed -e 's/^/REACT_APP_/' .env > ${PROJECT_NAME}/.env
cd ${PROJECT_NAME}
export PORT=${PROJECT_PORT}
yarn install
yarn run start
