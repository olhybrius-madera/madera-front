#!/bin/bash

set -euo pipefail

source .env

if [[ -d "$PROJECT_NAME" ]]; then
  exit
fi

npx create-react-app $PROJECT_NAME --template cra-template-pwa

PROJECT_SRC=$PROJECT_NAME/src

find templates/* -type d -exec bash -c 'mkdir $0/${1/templates\//}' $PROJECT_SRC {} \;
find templates/ -type f -exec bash -c 'dest_path=${1/templates\//} ; cp $1 $0/${dest_path%.template}' $PROJECT_SRC {} \;

sed -i 's/serviceWorkerRegistration\.unregister/serviceWorkerRegistration.register/' $PROJECT_SRC/index.js

dependencies=$(cat dependencies.txt)

cd $PROJECT_NAME

for dependency in $dependencies; do
  yarn add $dependency;
done

